import {Component} from "@angular/core";

@Component({
  selector: 'app-first-section',
  templateUrl: './first-section.component.html',
  styleUrls: ['./first-section.component.scss']
})
export class FirstSectionComponent {
  menu = null;
  bg = null;

  scrollTo(selector) {
    const scroll = window.pageYOffset;
    const elem = document.querySelector(selector).getBoundingClientRect().top;
    let start = null;
    requestAnimationFrame(step);
    function step(time) {
      const speed = 0.7;
      if (start === null) {
        start = time;
      }
      const progress = time - start,
        r = (elem < 0 ? Math.max(scroll - progress / speed, scroll + elem) : Math.min(scroll + progress / speed, scroll + elem));
      window.scrollTo(0, r);
      if (r !== scroll + elem) {
        requestAnimationFrame(step);
      }
    }
  }

  toggleMenu(event) {
    if (this.menu === null) {
      this.menu = event.currentTarget.nextElementSibling;
    }
    if (this.bg === null) {
      this.bg = event.currentTarget.querySelector('.box-bg');
    }
    this.menu.classList.toggle('active');
    this.bg.classList.toggle('active');
  }

  closeMenu() {
    this.menu.classList.remove('active');
    this.bg.classList.remove('active');
  }
}
