import {Component} from "@angular/core";

const products = {
  'items': [
    {
      'type': 'Child',
      'img': 'item1',
      'price': '3.20',
      'name': 'T-SHIRT',
      'options': ['Size', 'S', 'M', 'L', 'XL']
    }
    , {
      'type': 'Child',
      'img': 'item2',
      'price': '13.30',
      'name': 'Pants FORCLAZ',
      'options': ['Size', 'M', 'XL']
    }
    , {
      'type': 'Men',
      'img': 'item1',
      'price': '5.00',
      'name': 'T-SHIRT',
      'options': ['Size', 'XL']
    }
    , {
      'type': 'Women',
      'img': 'item2',
      'price': '3.21',
      'name': 'T-SHIRT',
      'options': ['Size', 'S', 'M', 'L', 'XL']
    }
    , {
      'type': 'Women',
      'img': 'item3',
      'price': '31',
      'name': 'backpack',
      'options': ['color', 'red', 'blue', 'black']
    }
    , {
      'type': 'Other',
      'img': 'item3',
      'price': '0',
      'name': 'car',
      'options': ['color', 'red', 'blue', 'black']
    }
  ]
};

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent {
  objectKeys = Object.keys;
  showCategory = {
    'men': true,
    'women': true,
    'child': false,
  };
  currentProducts = this.filterProduct(products.items);

  filterProduct(arr) {
    const productsArr = [];
    arr.forEach((item) => {
      if (this.showCategory[item.type.toLowerCase()] === true) {
        productsArr.push(item);
      }
    });
    return productsArr;
  }

  changeFilter(e) {
    const name = e.target.getAttribute('name');
    this.showCategory[name] = !this.showCategory[name];
    this.currentProducts = this.filterProduct(products.items);
  }

  clearFilter(e) {
    e.preventDefault();
    const input = e.target.querySelectorAll('input');
    for (const item of input) {
      if (item.getAttribute('checked') === null) {
        item.click();
      }
    }
    this.currentProducts = this.filterProduct(products.items);
  }

}
