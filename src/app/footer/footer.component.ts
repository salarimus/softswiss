import {Component} from '@angular/core';

const menu = {
  'footerMenu': [
    {
      'name': 'Credits',
      'link': 'https://softswiss.com/',
      'title': 'credits'
    },
    {
      'name': 'Privacy',
      'link': 'https://softswiss.com/',
      'title': 'privacy'
    },
    {
      'name': 'About',
      'link': 'https://www.softswiss.com/about-us/',
      'title': 'about us'
    },
    {
      'name': 'Contact',
      'link': 'https://www.softswiss.com/contact-us/',
      'title': 'contact us'
    }
  ]
};


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
  menu = menu.footerMenu;
}
